let display;

window.onload = () => {
	initButtons();

	const buttons = document.getElementsByTagName('button');
	display = document.getElementById('valoare');

	for (let i = 0; i < buttons.length; i++) {
		buttons[i].addEventListener('click', onButtonClicked);
	}

}

let isCalculating = false;

function initButtons() {
	const buttonContainer = document.getElementById('button-wrapper');
	for (let i = 0; i < 10; i++) {
		buttonContainer.appendChild(createButton(i));
	}

	const operationButtons = ['+', '-', '/', 'X', '=', 'C'];

	for (let item of operationButtons) {
		buttonContainer.appendChild(createButton(item));
	}
}

function createButton(value) {
	const button = document.createElement('button');
	button.innerText = value;
	if (isNaN(value)) {
		button.className = 'operation'
	}
	return button;
}


function onButtonClicked(evt) {
	const valoare = evt.target.innerText;

	if (!isNaN(+valoare)) {
		display.innerText += valoare;
	} else {
		if (isCalculating || valoare == '=' || valoare == 'C') {
			isCalculating = false;
			display.innerText = calculate();
		} else {
			isCalculating = true;
			display.innerText += valoare;
		}
	}
}

function calculate() {
	const value = document.getElementById('valoare').innerText.split('');
	let operationIndex = -1;

	for (let i = 0; i < value.length; i++) {
		if (isNaN(value[i])) {
			operationIndex = i;
			break;
		}
	}

	let number1 = '';

	for (let i = 0; i < operationIndex; i++) {
		number1 += value[i];
	}

	number1 = parseInt(number1);

	let number2 = '';

	for (let i = operationIndex + 1; i < value.length; i++) {
		number2 += value[i];
	}

	number2 = parseInt(number2);

	if (isNaN(number2)) {
		number2 = number1;
	}

	switch (value[operationIndex]) {
		case ('+'):
			return number1 + number2;
		case ('-'):
			return number1 - number2;
		case ('X'):
			return number1 * number2;
		case ('/'):
			return number1 / number2;
		default:
			return null;
	}
}